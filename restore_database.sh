#!/bin/bash

clear;

# Be om navn på sql fil som skal importeres
  echo Navn på .sql fil, f.eks backup.sql;
  read backup_sql_file;

# Gjør et søk etter filen
  result=$(find $HOME -name ${backup_sql_file} -print);

# Skriver ut sti til filen
  echo Fil funnet: ${result};

# Spør etter brukernavn på database
  echo Hvilken database hos Domeneshop skal du importere til?;
  read db;

# Spør etter database passord
  echo Skriv inn database passord:;
  read db_pass;

# Be om bekreftelse for å overskrive database
  echo Du vil overskrive dagens databasetabeller, vil du fortsette?;
  echo [Y][N];
  read overwrite;

# Hvis Y, importer .sql
  if [ "${overwrite}" = "Y" ]; then
  mysql -h ${db}.mysql.domeneshop.no -u${db} -p${db_pass} ${db} < ${result};
# Skriver ut avsluttende melding
  echo Databasen er nå importert. På gjensyn.;

  # Hvis N, avslut terminalvindu
  else echo Ingen ting ble importert. På gjensyn.;

fi

read exit;